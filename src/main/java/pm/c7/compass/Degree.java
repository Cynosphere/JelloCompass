package pm.c7.compass;

public class Degree {
    public String text;
    public int type;

    public Degree(String s, int t){
        text = s;
        type = t;
    }
}
