package pm.c7.compass;

import com.google.common.collect.Lists;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.IConfigElement;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.util.*;
import java.util.List;

@Mod(modid = JelloCompass.MODID, version = JelloCompass.VERSION, clientSideOnly = true, guiFactory = "pm.c7.compass.GuiFactory")
@SideOnly(Side.CLIENT)
public class JelloCompass {
    public static final String MODID = "jellocompass";
    public static final String VERSION = "0.0.1";

    public static JelloCompass INSTANCE = new JelloCompass();

    public static Configuration config;

    private Minecraft client;
    private boolean showConfig = false;

    private static boolean enabled;
    private static boolean rainbow;
    private static int rainbowSpeed;
    private static int hue;
    private static int sat;
    private static int val;

    private List<Degree> degrees = Lists.newArrayList();

    public JelloCompass() {
        this.client = Minecraft.getMinecraft();

        degrees.add(new Degree("N", 1));
        degrees.add(new Degree("195", 2));
        degrees.add(new Degree("210", 2));
        degrees.add(new Degree("NE", 3));
        degrees.add(new Degree("240", 2));
        degrees.add(new Degree("255", 2));
        degrees.add(new Degree("E", 1));
        degrees.add(new Degree("285", 2));
        degrees.add(new Degree("300", 2));
        degrees.add(new Degree("SE", 3));
        degrees.add(new Degree("330", 2));
        degrees.add(new Degree("345", 2));
        degrees.add(new Degree("S", 1));
        degrees.add(new Degree("15", 2));
        degrees.add(new Degree("30", 2));
        degrees.add(new Degree("SW", 3));
        degrees.add(new Degree("60", 2));
        degrees.add(new Degree("75", 2));
        degrees.add(new Degree("W", 1));
        degrees.add(new Degree("105", 2));
        degrees.add(new Degree("120", 2));
        degrees.add(new Degree("NW", 3));
        degrees.add(new Degree("150", 2));
        degrees.add(new Degree("165", 2));
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        ClientCommandHandler.instance.registerCommand(new ConfigCommand());

        MinecraftForge.EVENT_BUS.register(INSTANCE);
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        config = new Configuration(event.getSuggestedConfigurationFile());
        saveConfig();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e) {
        if (config.hasChanged()) {
            config.save();
        }
    }

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
        if (event.modID.equals(MODID)) {
            saveConfig();
        }
    }

    @SubscribeEvent
    public void onTick(TickEvent.ClientTickEvent event) {
        if (showConfig) {
            client.displayGuiScreen(new ConfigGui(null));
            showConfig = false;
        }
    }

    public static void saveConfig() {
        enabled = config.getBoolean("Enable Compass", "main", true, null);
        rainbow = config.getBoolean("Rainbow", "main", false, "Rainbow compass text. Saturation and Value effect this");
        rainbowSpeed = config.getInt("Rainbow Speed", "main", 2, 1, 10, "Rainbow speed");
        hue = config.getInt("Hue", "main", 0, 0, 360, null);
        sat = config.getInt("Saturation", "main", 0, 0, 100, null);
        val = config.getInt("Value", "main", 100, 0, 100, null);

        config.save();
    }

    @SubscribeEvent
    public void drawCompass(RenderGameOverlayEvent event) {
        if (event.type == RenderGameOverlayEvent.ElementType.TEXT && this.client.ingameGUI != null && event.type != RenderGameOverlayEvent.ElementType.DEBUG && enabled) {
            GlStateManager.disableAlpha();
            GlStateManager.enableBlend();

            ScaledResolution res = new ScaledResolution(client);
            float center = res.getScaledWidth() / 2.0f;

            int count = 0;
            int cardinals = 0;
            int subCardinals = 0;
            int markers = 0;
            float yaw = this.client.thePlayer != null ? (this.client.thePlayer.rotationYaw % 360) * 2 + 360 * 3 : 0;

            for (int i = 0; i < 3; i++) {
                for (Degree d : degrees) {
                    float location = center + (count * 30) - yaw;
                    float completeLocation = location - client.fontRendererObj.getStringWidth(d.text) / 2.0F;

                    Color ccolor = Color.getHSBColor(rainbow ? ((System.nanoTime() * rainbowSpeed) / 9E9F + ((count - 1) * 0.01F)) % 1F : hue / 360F, sat / 100F, val / 100F);
                    Color ocolor = opacity(ccolor.getRed(), ccolor.getGreen(), ccolor.getBlue(), res.getScaledWidth(), completeLocation);
                    int alpha = ocolor.getAlpha();
                    int opacity = ocolor.getRGB();

                    if (d.type == 1 && alpha > 3) {
                        GlStateManager.color(1, 1, 1, 1);

                        GlStateManager.pushMatrix();
                        GlStateManager.scale(2.0F, 2.0F, 2.0F);
                        client.fontRendererObj.drawStringWithShadow(d.text, (int) completeLocation >> 1, (int) (25 - client.fontRendererObj.FONT_HEIGHT / 2.0F) >> 1, opacity);
                        GlStateManager.scale(0.5F, 0.5F, 0.5F);
                        GlStateManager.popMatrix();

                        cardinals++;
                    }

                    if (d.type == 2 && alpha > 3) {
                        GlStateManager.color(1, 1, 1, 1);
                        Gui.drawRect((int) (location - 0.5F), -75 + 100 + 4, (int) (location + 0.5F), -75 + 105 + 4, opacity);
                        GlStateManager.color(1, 1, 1, 1);

                        GlStateManager.pushMatrix();
                        GlStateManager.scale(0.5F, 0.5F, 0.5F);
                        client.fontRendererObj.drawStringWithShadow(d.text, (int) (location - (client.fontRendererObj.getStringWidth(d.text) / 2.0F) / 2.0F) << 1, (int) (37.5F) << 1, opacity);
                        GlStateManager.scale(2.0F, 2.0F, 2.0F);
                        GlStateManager.popMatrix();

                        markers++;
                    }

                    if (d.type == 3 && alpha > 3) {
                        GlStateManager.color(1, 1, 1, 1);
                        client.fontRendererObj.drawStringWithShadow(d.text, completeLocation, 25 + client.fontRendererObj.FONT_HEIGHT / 2.0f - client.fontRendererObj.FONT_HEIGHT / 2.0f, opacity);
                        subCardinals++;
                    }

                    count++;
                }
            }
        }
    }

    private Color opacity(int r, int g, int b, float resolution, float offset) {
        float offs = 255 - Math.abs(resolution / 2.0f - offset) * 1.8f;
        Color c = new Color(r, g, b, (int) Math.min(Math.max(0, offs), 255));

        return c;
    }

    public class ConfigCommand extends CommandBase {
        @Override
        public String getCommandName() {
            return "jcconfig";
        }

        @Override
        public String getCommandUsage(ICommandSender sender) {
            return "/jcconfig";
        }

        @Override
        public void processCommand(ICommandSender sender, String[] args) throws CommandException {
            showConfig = true;
        }

        @Override
        public int getRequiredPermissionLevel() {
            return 0;
        }
    }
}