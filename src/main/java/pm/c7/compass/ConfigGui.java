package pm.c7.compass;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.config.GuiConfig;

public class ConfigGui extends GuiConfig
{
    public ConfigGui(final GuiScreen parent) {
        super(parent, new ConfigElement(JelloCompass.config.getCategory("main")).getChildElements(), JelloCompass.MODID, false, false, GuiConfig.getAbridgedConfigPath(JelloCompass.config.getConfigFile().getPath()));
    }
}
